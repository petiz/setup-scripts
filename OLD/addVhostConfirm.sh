#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

if [[ ! -v force_vhost_exists ]]
then
	while [ true ]
	do
		echo "Does the vhost already exist? (Enter \"no\" for new entry OR \"yes\" for finishing the script)"
		read vhost_exist

		if [ "$vhost_exist" == "no" ]	
		then
			#cd "$(dirname "$0")"
            source "$(dirname "$0")/addVhost.sh"
			break
		
		elif [ "$vhost_exist" == "yes" ]
		then
			echo "OK! Vhost already exists"
			break
		else
			echo "Please enter \"yes\" or \"no\""
		fi
	done
fi
