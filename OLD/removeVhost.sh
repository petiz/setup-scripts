#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi


webroot="/home/$_user/htdocs"

echo "Please enter the foldername for the project you want to REMOVE WITHOUT final slash (/)"
read projectfolder

rm -rf /etc/apache2/sites-enabled/$projectfolder.conf 2> /dev/null
rm -rf /etc/apache2/sites-available/$projectfolder.conf 2> /dev/null
rm -rf "$webroot/$projectfolder" 2> /dev/null

/etc/init.d/apache2 restart

echo "Done!"
