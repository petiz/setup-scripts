#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi


while [ true ]; do
	
	echo "Please your enviroment. Press \"2\" for Mint (Desktop) or \"7\" for Debian (Server)"
	read enviroment_id
	
	if [ $enviroment_id == "2" ]; then
		enviroment="desktop"
		php_versions=("5.6" "7.2" "7.3" "7.4")
		break
	elif [ $enviroment_id == "7" ]; then
		enviroment="server"
		php_versions=("5.6" "7.2")
		break
	fi
done

if [ "$(cat /etc/os-release | grep Mint)" != "" ]; then
	os="mint"
elif [ "$(cat /etc/os-release | grep Ubuntu)" != "" ]; then
	os="mint"
elif [ "$(cat /etc/os-release | grep Debian)" != "" ]; then
	os="debian"

else
	echo "No compatible linux-distribution found! Exit"
	exit
fi


# ----------------- remove necessary apache-packages -----------------
if [ -f ~/.install_flags/setup_apache_enviroment/apache_packages ]; then
	echo "- removing necessary apache-packages -"
	sudo apt-get purge -y apache2 libapache2-mod-fcgid > /dev/null
	sudo apt-get purge -y mariadb-server > /dev/null
	rm ~/.install_flags/setup_apache_enviroment/apache_packages
else
	echo "Apache packages has already been removed"
fi

# ----------------- remove necessary php-packages -----------------
if [ -f "/home/$_user/.install_flags/setup_apache_enviroment/php_packages" ]; then
	echo "removing global php-packages incl. redis-server"
	sudo apt-get purge -y php-common php-imagick php-redis redis > /dev/null
	
	if [ $enviroment == "desktop" ]; then
		echo "removing x-debug"
		sudo apt-get purge -y php-xdebug > /dev/null
	fi
	
	rm "/home/$_user/.install_flags/setup_apache_enviroment/php_packages"
fi

for v in "${php_versions[@]}"
do
	if [  -f "/home/$_user/.install_flags/setup_apache_enviroment/php${v}_packages" ]; then
		echo "remove essential packages for php ${v}"
		sudo apt-get purge -y "php$v" "php$v-fpm" "php$v-common" "php$v-mysql" "php$v-mbstring" "php$v-opcache" "php$v-curl" "php$v-json" "php$v-xml" "php$v-cli" "php$v-gd" "php$v-zip" "php$v-intl" "php$v-readline" "php$v-bcmath" "php$v-soap" > /dev/null
		rm "/home/$_user/.install_flags/setup_apache_enviroment/php${v}_packages"
		rm "/home/$_user/.install_flags/setup_apache_enviroment/php${v}_settings"

	else
		echo "Packages for PHP has been already removed (version $v)"
	fi
done


	
