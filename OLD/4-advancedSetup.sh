#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

# DISABLED
touch ~/.setup_flags/desktop_enviroment/oem_kernel
if [ ! -f ~/.setup_flags/desktop_enviroment/oem_kernel ]; then
	echo "Installing newest oem kernel"
	sudo apt-get install -y linux-oem-22.04 > /dev/null
	touch ~/.setup_flags/desktop_enviroment/oem_kernel
fi

# ----------------- Backup Setup  -----------------
#DISABLED
touch ~/.setup_flags/desktop_enviroment/backup
if [ ! -f ~/.setup_flags/desktop_enviroment/backup ]; then
	mkdir -p "/home/$_user/.backup"
	echo "add backup mountpoint"
	echo "UUID=732215c8-7ba4-4368-9ca1-53fdc9403e45 /home/pbecker/.backup ext4 rw,user,exec 0 0" | sudo tee -a /etc/fstab
	(crontab -l | grep . ; echo -e "0 * * * * /home/pbecker/Tools/Shell/backup_cronjob.sh\n") | crontab -
	sudo mount -a
	touch ~/.setup_flags/desktop_enviroment/backup
else
	echo "backup mountpount has already been added"
fi

# ----------------- GRUB Changes -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/grub
if [ ! -f ~/.setup_flags/desktop_enviroment/grub ]; then
	echo "Modifying Grub Bootloader"
	#sudo sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"acpi_backlight=vendor amd_iommu=on iommu=pt i8042.reset i8042.nomux i8042.nopnp i8042.noloop acpi=strict\"/g" "/etc/default/grub"
	sudo sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"acpi_backlight=vendor amd_iommu=on acpi=strict\"/g" "/etc/default/grub"

	sudo update-grub
	touch ~/.setup_flags/desktop_enviroment/grub
else
	echo "Grub Bootloader has already been modified"
fi


# ----------------- Bluetooth XBOX-One Controller -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/bluetooth_xbox
if [ ! -f ~/.setup_flags/desktop_enviroment/bluetooth_xbox ]; then
	echo "Disable Bluetooth ERTM for Xbox-One-Controller"
	sudo apt-get install -y sysfsutils 
	if [ -f /etc/sysfs.conf ]; then
		echo "module/bluetooth/parameters/disable_ertm = 1" | sudo tee -a /etc/sysfs.conf
		sudo service sysfsutils restart
		touch ~/.setup_flags/desktop_enviroment/bluetooth_xbox
	fi
else
	echo "Bluetooth ERTM for Xbox-One Controller has already been disabled"
fi

# ----------------- OBS Studio -----------------
#DISABLED
touch ~/.setup_flags/desktop_enviroment/obs
if [ ! -f ~/.setup_flags/desktop_enviroment/obs ]; then
	echo "Install OBS Studio"
	sudo add-apt-repository -y ppa:obsproject/obs-studio
	sudo apt update
	sudo apt install -y obs-studio
	touch ~/.setup_flags/desktop_enviroment/obs
else
	echo "OBS Studio has already been installed"
fi


# ----------------- Install Samba -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/samba
if [ ! -f ~/.setup_flags/desktop_enviroment/samba ]; then
	echo "- Modifiying samba -"
	mkdir -p ~/Share
	samba_config="\n\n[Share]\ncomment = Share\npath = /home/$_user/Share\nbrowseable = yes\nread only = no\nguest ok = no"
	printf "$samba_config" | sudo tee -a "/etc/samba/smb.conf" > /dev/null
	sudo smbpasswd -a "$_user"
	touch ~/.setup_flags/desktop_enviroment/samba
else
	echo "Samba has already been modified"
fi

# ----------------- Install Chrome -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/chrome
if [ ! -f ~/.setup_flags/desktop_enviroment/chrome ]; then
	echo "- Installing chrome -"
	cd "/home/$_user/Downloads"
	wget -P ~/Downloads https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	if [ -f  ~/Downloads/google-chrome-stable_current_amd64.deb ]; then
		sudo gdebi --non-interactive ~/Downloads/google-chrome-stable_current_amd64.deb
		cd ~
		touch ~/.setup_flags/desktop_enviroment/chrome
	else
		echo "Error: Chrome Download"
		exit
	fi
	cd ~
else
	echo "Chrome has already been installed"
fi


# ----------------- Install Steam -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/steam
if [ ! -f ~/.setup_flags/desktop_enviroment/steam ]; then
	echo "- Installing zoom -"
	cd "/home/$_user/Downloads"
	wget -P ~/Downloads https://cdn.akamai.steamstatic.com/client/installer/steam.deb
	if [ -f  ~/Downloads/steam.deb ]; then
		sudo gdebi --non-interactive ~/Downloads/steam.deb
		touch ~/.setup_flags/desktop_enviroment/steam
	else
		echo "Error: Steam Download"
		exit
	fi
	cd ~
else
	echo "Steam has already been installed"
fi

# ----------------- install Phpstorm -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/phpstorm
if [ ! -f ~/.setup_flags/desktop_enviroment/phpstorm ]; then
	echo "- Installing phpstorm -"
	cd "/home/$_user/Downloads"
	wget -P ~/Downloads https://download.jetbrains.com/webide/PhpStorm-2021.2.3.tar.gz
	if [ -f  ~/Downloads/PhpStorm-2021.2.3.tar.gz ]; then
		tar -xf ~/Downloads/PhpStorm-2021.2.3.tar.gz
		if [ ! -d ~/phpstorm ]; then
			mv PhpStorm-212* ~/phpstorm
		fi
		cd ~
		touch ~/.setup_flags/desktop_enviroment/phpstorm
	else
		echo "Error: Phpstorm Download"
		exit
	fi
else
	echo "Phpstorm has already been installed"
fi


# ----------------- File-Manager Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/file-manager
if [ ! -f ~/.setup_flags/desktop_enviroment/file-manager ]; then
	echo "- switching file-manager from thunar to nemo -"
	sudo apt-get -y install nemo
	touch ~/.setup_flags/desktop_enviroment/file-manager
else
	echo "File-manager has already been switched from thunar to nemo"
fi

# ----------------- Virtualbox Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/virtualbox
if [ ! -f ~/.setup_flags/desktop_enviroment/virtualbox ]; then
	echo "- install virtualbox && add current user to vboxusers-group -"
	sudo apt-get install -y build-essential libssl-dev linux-headers-`uname -r`
	sudo groupadd vboxusers
	sudo usermod -aG vboxusers $(whoami)
	wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
	echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian focal contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
	sudo apt update
	sudo apt install -y virtualbox
	touch ~/.setup_flags/desktop_enviroment/virtualbox
else
	echo "Virtualbox-configuration has already been done"
fi

# ----------------- Avidemux Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/avidemux
if [ ! -f ~/.setup_flags/desktop_enviroment/avidemux ]; then
	echo "- install avidemux"
	sudo apt-add-repository -y ppa:ubuntuhandbook1/avidemux
	sudo apt-get update
	sudo apt-get install -y avidemux-common avidemux-qt
	touch ~/.setup_flags/desktop_enviroment/avidemux
else
	echo "Avidemux has already been installed"
fi

# ----------------- Unetbootin Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/unetbootin
if [ ! -f ~/.setup_flags/desktop_enviroment/unetbootin ]; then
	echo "- install unetbootin"
	sudo add-apt-repository -y ppa:gezakovacs/ppa
	sudo apt-get update
	sudo apt-get install -y unetbootin
	touch ~/.setup_flags/desktop_enviroment/unetbootin
else
	echo "Unetbootin has already been installed"
fi

# ----------------- Lutris Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/lutris
if [ ! -f ~/.setup_flags/desktop_enviroment/lutris ]; then
	echo "- install lutris"
	
	sudo apt-get -y install libva-dev:i386
	sudo dpkg --add-architecture i386
	sudo add-apt-repository ppa:lutris-team/lutris
	sudo apt update
	sudo apt install -y lutris
	touch ~/.setup_flags/desktop_enviroment/lutris
else
	echo "Lutris has already been installed"
fi

# ----------------- Docker -----------------
# DISABLED
touch ~/.setup_flags/development_enviroment/docker

if [ ! -f ~/.setup_flags/development_enviroment/docker ]; then
	echo "- install docker -"
	sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo apt-key fingerprint 0EBFCD88
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
	sudo apt-get update
	sudo apt-get install -y docker-ce docker-ce-cli containerd.io
	touch ~/.setup_flags/development_enviroment/docker
else
	echo "docker has already been installed"
fi


# ----------------- Elasticsearch 6 -----------------
# DISABLED
touch ~/.setup_flags/development_enviroment/elasticsearch6

if [ ! -f ~/.setup_flags/development_enviroment/elasticsearch6 ]; then
	sudo apt-get install -y openjdk-8-jre apt-transport-https
	#wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
	echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
	sudo apt-get update && sudo apt-get install -y elasticsearch
	sudo usermod -aG elasticsearch "$USER"
	sudo systemctl enable elasticsearch.service
	sudo systemctl start elasticsearch.service
	sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-phonetic
	sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu
	sudo service elasticsearch restart
	touch ~/.setup_flags/development_enviroment/elasticsearch6
else
	echo "elasticsearch 6 has already been installied"
fi
