#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

# Enter Webroot always as absolute path WITHOUT final slash ( / )

webroot="/home/$_user/htdocs"

force_new_db=true

# Default database connection (User: root, password: root is default for MAMP (Pro)

if [ ! -f ~/.mylogin.cnf ]; then
	echo "There is no Login-Information for your database available. Please enter the root-password for your database"
	echo "Please run the following command in your unix-shell and enter your sql root-password"
	echo "mysql_config_editor set --login-path=client --host=localhost --user=root --password"
fi

table_prefix=''

source "$(dirname "$0")/include/db-credentials.sh"

if [[ ! -v projectfolder ]]; then
	echo "Please enter the foldername for this project inside the webroot WITHOUT final slash (/)"
	read projectfolder
fi
if [[ ! -v subfolder ]]; then
	echo "Please enter the subfolder-name for this project where the vhost points to  (default: $default_subfolder)"
	read subfolder
fi
if [[ ! -v database ]]; then
	echo "Please enter the Name of the Database, you want to create for this project (default: $projectfolder)"
	read database
fi
if [[ ! -v vhost ]]; then
	echo "Please enter name of vhost WITHOUT \"http(s)://\" (Example: testsystem.localhost)"
	read vhost 
fi
if [ "$database" == "" ]; then
  database="$projectfolder"
fi
if [ "$subfolder" == "" ]; then
  subfolder="$default_subfolder"
fi
