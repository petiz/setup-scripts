#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

devicePath="/media/${_user}/backup"

if [ ! -d ${devicePath} ]
then
	echo "kein backup-device gefunden"
	exit
fi

if [ ! -d ${devicePath}/${_user}/.config ]
then
	mkdir -p ${devicePath}/${_user}/.config
fi

path="${devicePath}/${_user}/"

cp ~/Setup-Scripts/1-restoreSyncBackup.sh ${path}/
cp ~/Setup-Scripts/2-initEnviroment.sh ${path}/
cp ~/Setup-Scripts/3-setupDevelopmentEnviroment.sh ${path}/

# files
rsync -avz --delete ~/.lx_config.ovpn ${path}/
rsync -avz --delete ~/.config.ovpn ${path}/

# Small folders
rsync -avz --delete ~/.pwfile ${path}/
rsync -avz --delete ~/.thunderbird ${path}/
rsync -avz --delete ~/.ssh ${path}/
rsync -avz --delete ~/Composer-Phar-Files ${path}/
rsync -avz --delete ~/Setup-Scripts ${path}/
rsync -avz --delete ~/Tools ${path}/

rsync -avz --delete ~/.config/composer ${path}/.config/
rsync -avz --delete ~/.config/VirtualBox ${path}/.config/

# Large folders
rsync -avz --delete ~/Musik ${path}/
rsync -avz --delete ~/Cloud ${path}/
rsync -avz --delete --exclude 'var/*' ~/htdocs ${path}/
rsync -avz --delete ~/VirtualBox\ VMs ${path}/



