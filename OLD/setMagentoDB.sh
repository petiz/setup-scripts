#!/bin/bash

# Change to the directory of the Shell-Script
cd "$(dirname "$0")"



if [ -f "app/etc/local.xml" ]
then
	sqlhost=$(cat app/etc/local.xml | grep host | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	sqluser=$(cat app/etc/local.xml | grep username | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	sqlpwd=$(cat app/etc/local.xml | grep password | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	database=$(cat app/etc/local.xml | grep dbname | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
else
	sqlhost='localhost'
	sqluser='root'
	sqlpwd='xyz' 
	echo "Please enter the Name of the Magento-Database:"
	read database
fi

echo "Please enter the Name of the VHost WITHOUT http(s):// (Example: testhost.localhost)"
read vhost 



changes="
  update core_config_data set value = 'http://$vhost/' where path = 'web/unsecure/base_url';
  update core_config_data set value = 'http://$vhost/' where path = 'web/secure/base_url'; 
 
  update core_config_data set value = '0' where path = 'dev/css/merge_css_files';
  update core_config_data set value = '0' where path = 'dev/js/merge_files';
  update core_config_data set value = '0' where path = 'admin/security/use_form_key';
  update core_config_data set value = '0' where path = 'web/secure/use_in_frontend';
  update core_config_data set value = '0' where path = 'web/secure/use_in_adminhtml';
  
  update core_config_data set value = '1' where path = 'catalog/frontend/flat_catalog_category';
  update core_config_data set value = '1' where path = 'catalog/frontend/flat_catalog_product';
  
  update core_config_data set value = '' where path = 'sales_email/order/copy_to';
  update core_config_data set value = '' where path = 'sales_email/invoice/copy_to';
  update core_config_data set value = '' where path = 'sales_email/creditmemo/copy_to';
  update core_config_data set value = '' where path = 'sales_email/creditmemo_comment/copy_to';
  
  update core_cache_option set value = '0';
"

echo "Setting Test-Enviroment Settings in Database ..."


ERR=$((mysql -h $sqlhost -u $sqluser  -p$sqlpwd $database -e "$changes") 2>&1)


if [ "$ERR" != "" ]
then
	echo $ERR
	echo "Database-Values are not valid or Database is not running. Script stops now ..."
	exit
else
	echo "done!"
fi 


echo "done"
