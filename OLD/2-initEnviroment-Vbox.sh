#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

mkdir -p "/home/$_user/.setup_flags/desktop_enviroment"
mkdir -p "/home/$_user/Downloads"


# ----------------- Idle Setup (Idle also when external screens are connected) -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/idle_setup ]; then
	echo "Idle Setup"
	if [ -f /etc/systemd/logind.conf ]; then
		echo "HandleLidSwitchExternalPower=suspend" | sudo tee -a /etc/systemd/logind.conf
		echo "HandleLidSwitchDocked=suspend" | sudo tee -a /etc/systemd/logind.conf
		touch ~/.setup_flags/desktop_enviroment/idle_setup
	fi
else
	echo "Idle-Setup has already been configured"
fi

# ----------------- Reduce timeout  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/timeout ]; then
	echo "Reduce timeout for processes during shutdown to 10s"
	if [ -f /etc/systemd/system.conf ]; then
		echo "DefaultTimeoutStopSec=10s" | sudo tee -a /etc/systemd/system.conf
		systemctl daemon-reload
		touch ~/.setup_flags/desktop_enviroment/timeout
	fi
else
	echo "Shutdown timeout has been already reduced"
fi

# ----------------- SWAP Changes  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/swap_size ]; then
	echo "- Increasing Swap-Size to 32GB && Swapiness to 80 -"
	sudo swapoff -a
	sudo dd if=/dev/zero of=/swapfile bs=1024M count=32
	sudo chmod 0600 /swapfile
	sudo mkswap /swapfile
	sudo swapon /swapfile
	echo "/swapfile swap swap defaults 0 0" | sudo tee -a /etc/fstab
	echo "vm.swappiness=30" | sudo tee -a /etc/sysctl.conf
	sudo sysctl vm.swappiness=30
	touch ~/.setup_flags/desktop_enviroment/swap_size

else
	echo "Swap Configuration has already been modified"
fi

if [ ! -f ~/.setup_flags/desktop_enviroment/bloatware_removed ]; then
	echo "Remove bloatware-desktop-apps"
    sudo apt-get purge -y rhythmbox rhythmbox-plugins rhythmbox-plugin-tray-icon hexchat celluloid transmission-common transmission-gtk pix pix-data pix-dbg xed xed-common > /dev/null
	touch ~/.setup_flags/desktop_enviroment/bloatware_removed
fi



if [ ! -f ~/.setup_flags/desktop_enviroment/setup_scripts ]; then

	if [ ! -d ~/Setup-Scripts ]; then
		echo "Please mount first"
		exit
	fi
	
	cd ~/Setup-Scripts
	
	ERR=$((git clone -q https://gitlab.com/petiz/setup-scripts.git .) 2>&1) 
	
	if [ "$ERR" != "" ]; then
		echo "Error: $ERR"
		rm -r ~/Setup-Scripts 2> /dev/null
		exit
	else
		echo "Setup-Scripts-Repository has been successfully cloned"
		touch ~/.setup_flags/desktop_enviroment/setup_scripts
	fi
else
	echo "Setup-Scripts-Repostory has already been cloned"
fi
