#!/bin/bash

default_subfolder='shopware'
source "$(dirname "$0")/initAddFramework.sh"


echo "Trying to find Shopware-Installation in: $webroot/$projectfolder/$subfolder/ ..."

if [ -f "$webroot/$projectfolder/$subfolder/app/Mage.php" ]
then
	webpath="$webroot/$projectfolder/$subfolder"
else
	echo "Webroot and/or projectfolder values are wrong. Script stops now ..."
	exit
fi

if [ -d "$webpath" ] 
then
   echo "Cleaning Cache- and Session Data ..."	
   rm -rf $webpath/var/cache/*
   rm -rf $webpath/var/session/*
   rm -rf $webpath/var/locks/*
   echo "Done!"
else
	echo "Webroot and/or projectfolder values are wrong. Script stops now ..."
	exit
fi

if [ -f "$webpath/app/etc/local.xml" ]
then
	table_prefix=$(cat $webpath/app/etc/local.xml | grep table_prefix | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	echo "Table-Prefix set to $table_prefix"
	echo "Done!"
fi

source "$(dirname "$0")/addDatabase.sh"
source "$(dirname "$0")/include/magento1/db-develop-changes.sh"
source "$(dirname "$0")/include/magento1/db-admin-exists.sh"

#if [ -f "$webroot/$projectfolder/additional.sql" ]; then
#	changes=$(<"$webroot/$projectfolder/additional.sql")
#	mysql --login-path=client $database -e "$changes"
#fi

echo "Updating local.xml"

if [ ! -f "$webpath/app/etc/local.xml" ]
then

	if [ -f "$webpath/app/etc/local.xml.dev" ]
	then
		echo "Copying local.xml.dev to local.xml"
		cp "$webpath/app/etc/local.xml.dev" "$webpath/app/etc/local.xml"
		localXmlType="1"
	elif [ -f "$webpath/app/etc/local.xml.local" ] 
	then
		echo "Copying local.xml.local to local.xml"
		cp "$webpath/app/etc/local.xml.local" "$webpath/app/etc/local.xml"
		localXmlType="1"
	else
		echo "Copying default local.xml to project"
		cp local.xml.template "$webpath/app/etc/local.xml"
		localXmlType="2"
	fi
	
else
	localXmlType="1"
fi

cd "$webpath/app/etc"
chown "$_user:$_user" local.xml

if [ "$localXmlType" == "1" ]
then
	sed -i -e "s,<host><[^<]*</host>,<host><![CDATA[$sqlhost]]></host>," local.xml
	sed -i -e "s,<username><[^<]*</username>,<username><![CDATA[$sqluser]]></username>," local.xml
	sed -i -e "s,<password><[^<]*</password>,<password><![CDATA[$sqlpwd]]></password>," local.xml
	sed -i -e "s,<dbname><[^<]*</dbname>,<dbname><![CDATA[$database]]></dbname>," local.xml
else
	sed -i -e "s,{{db_host}},<![CDATA[$sqlhost]]>," local.xml
	sed -i -e "s,{{db_user}},<![CDATA[$sqluser]]>," local.xml
	sed -i -e "s,{{db_pass}},<![CDATA[$sqlpwd]]>," local.xml
	sed -i -e "s,{{db_name}},<![CDATA[$database]]>," local.xml
fi

echo "Done!"

if [ -f "$webpath/.htaccess.dev" ]
then
	ln -fs "$webpath/.htaccess.dev" "$webpath/.htaccess"
fi

source "$(dirname "$0")/addVhostConfirm.sh"

echo "----------------------------"
echo "All Done!"

