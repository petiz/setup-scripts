#!/bin/bash

dbdump="db.sql"
dump_path=""

if [ -f "$webroot/$projectfolder/$dbdump" ]
then
	dump_path="$webroot/$projectfolder/$dbdump"
elif [ -f "$webroot/$projectfolder/dump/$dbdump" ]
then
	dump_path="$webroot/$projectfolder/dump/$dbdump"
fi

if [ "$dump_path" != "" ]
then
	
	echo "Checking Database Connection ..."
	
	ERR=$((mysql --login-path=client -e "") 2>&1)
	
	if [ "$ERR" != "" ]
	then
		
		echo $ERR
		echo "Database-Values are not valid or Database is not running. Script stops now ..."
		echo "If you entered a wrong root-password, you should delete the .mylogin.cnf in your home-folder"
		exit
		
	else
		
		echo "Done!"
		
		if [[ ! -z $database || $database == "" || $database == *['!'@#\$%^\&*()\-+]* ]]
		then
			database=""
			while [ true ]
			do
				echo "Please enter a valid name for the database (no special characters like \"-\"):"
				read database
				if [[ $database == *['!'@#\$%^\&*()\-+]* ]]
				then
					echo "Invalid characters in $database"
				else
					echo "Database name seems valid"
					break
				fi
			done
		fi
		
		while [ true ]
		do
			echo "Creating new database \"$database\" ..."
			add_database="CREATE DATABASE $database"
		
			db_exists=$((mysql --login-path=client -e "$add_database") 2>&1)

			if [ "$db_exists" != "" ]
			then

				if [[ ! -v force_new_db ]]
				then
					echo "$db_exists"
					echo "This database already exists. Do you want to drop and proceed? (yes/no) "
					read proceed
				fi
				
				if [[ "$proceed" == "yes" ||  -v force_new_db ]]
				then
				
					echo "Dropping existing database \"$database\" ..."
					drop_database="DROP DATABASE $database"
					delete=$((mysql --login-path=client -e "$drop_database") 2>&1)
					
					if [ "$delete" != "" ]
					then
						
						echo "Database Drop Error: $delete"
						exit 
					fi
					
				else
					
					echo "Please drop database first or delete the SQL-Dump to proceed."
					echo "Script stops now ..."
					exit
				fi	 
				
			else
			
				echo "Done!"
				echo "Importing SQL-Dump. This could take a while. Please wait ..."
				mysql --login-path=client $database < "$dump_path"
				echo "Done!"
				break
			fi
		done
	fi
	
else

	echo "SQL-Dump ($dump_path) not found , did you already import the SQL-Dump? (yes / no )"
	read already_imported
	
	if [ "$already_imported" != "yes" ]
	then
		echo "Please copy the SQL-Dump in your projectfolder or import it manually first. Script stops now ..."
		exit
	fi	 
fi
