#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi
	
if [ ! -d ~/.install_flags/additional_modifications ]; then
	mkdir -p ~/.install_flags/additional_modifications
fi

if [ ! -f ~/.install_flags/additional_modifications/tools ]; then
    if [ -d ~/.ssh ]; then
        mkdir -p ~/Tools
        git clone git@gitlab.petiz.de:pbecker/tools.git ~/Tools
        if [ -f ~/Tools/successfully_cloned ]; then
            touch ~/.install_flags/additional_modifications/tools
        else
			rm -rf ~/Tools
            echo "Error: Tools-Repository has not been cloned"
            exit     
        fi
    else
        echo "Error: You have to copy the .ssh-folder first"
        exit
    fi
else
	echo "Tools Repository has already been cloned"
fi

if [ ! -f ~/.install_flags/additional_modifications/setup_scripts_ssh ]; then
    if [ -d ~/.ssh ]; then
        mkdir -p ~/Setup-Scripts-SSH
        git clone git@gitlab.petiz.de:pbecker/setup-scripts.git ~/Setup-Scripts-SSH
        if [ -f ~/Setup-Scripts-SSH/successfully_cloned ]; then
            touch ~/.install_flags/additional_modifications/setup_scripts_ssh
        else
			rm -rf ~/Setup-Scripts-SSH
            echo "Error: SetupScripts-SSH-Repository has not been cloned"
            exit     
        fi
    else
        echo "Error: You have to copy the .ssh-folder first"
        exit
    fi
else
	echo "Setup-Scripts Repository has already been cloned"
fi

# ----------------- Leonex Aliases -----------------
if [ ! -f ~/.install_flags/additional_modifications/aliases_leonex ]; then
    additional_aliases="\n\nif [ -f /home/$_user/.bash_aliases_leonex ]; then\n    . /home/$_user/.bash_aliases_leonex\nfi"
    printf "$additional_aliases" >> ~/.bashrc
	touch ~/.install_flags/additional_modifications/aliases_leonex
else
    echo "aliases-file for leonex has already been added"
fi

# ----------------- Aliases for Tools -----------------
if [ ! -f ~/.install_flags/additional_modifications/aliases_tools ]; then
    additional_aliases="\n\nif [ -f /home/$_user/Tools/Shell/aliases.sh ]; then\n    . /home/$_user/Tools/Shell/aliases.sh\nfi"
    printf "$additional_aliases" >> ~/.bashrc
	touch ~/.install_flags/additional_modifications/aliases_tools
else
    echo "aliases-file for tools has already been added"
fi

# ----------------- add repository for seafile -----------------
if [ ! -f ~/.install_flags/additional_modifications/seafile_repository ]; then
    echo "- add repository for seafile -"
    sudo add-apt-repository -y ppa:seafile/seafile-client
	touch ~/.install_flags/additional_modifications/seafile_repository
else
    echo "seafile repository already has been added"
fi

# ----------------- copy local .bashrc to root-folder -----------------
if [ ! -f ~/.install_flags/additional_modifications/bashrc_to_root ]; then
    echo "- copy local .bashrc to root-folder -"
    sudo cp "/home/$_user/.bashrc" /root/.bashrc
    sudo chown root:root /root/.bashrc
	touch ~/.install_flags/additional_modifications/bashrc_to_root
else
    echo "bashrc has already been copied to root home-directory"
fi

# ----------------- install additional common software -----------------
if [ ! -f ~/.install_flags/additional_modifications/common_software_additional ]; then
echo "- install additional common software -"
    sudo apt-get update
    sudo apt-get dist-upgrade -y
    sudo apt-get install -y vlc seafile-gui audacious
	touch ~/.install_flags/additional_modifications/common_software_additional
else
    echo "additional common software has already been installed"
fi

# ----------------- install slack -----------------
if [ ! -f ~/.install_flags/additional_modifications/slack ]; then
	echo "- install slack -"
	wget -P ~/Downloads https://downloads.slack-edge.com/linux_releases/slack-desktop-3.3.8-amd64.deb
	if [ -f  ~/Downloads/slack-desktop-3.3.8-amd64.deb ]; then
		sudo gdebi --non-interactive "/home/$_user/Downloads/slack-desktop-3.3.8-amd64.deb"
		touch ~/.install_flags/additional_modifications/slack
	else
		echo "Error: Slack Download"
		exit
	fi
else
	echo "Slack has already been installed"
fi

# ----------------- install steam -----------------
if [ ! -f ~/.install_flags/additional_modifications/steam ]; then
    echo "- install steam -"
    wget -P ~/Downloads https://steamcdn-a.akamaihd.net/client/installer/steam.deb
    if [ -f  ~/Downloads/steam.deb ]; then
		sudo gdebi --non-interactive "/home/$_user/Downloads/steam.deb"
		touch ~/.install_flags/additional_modifications/steam
	else
		echo "Error: Download Steam"
		exit
	fi
else
    echo "steam has already been installed"
fi



