#!/bin/bash



if [ -f "app/etc/local.xml" ]
then
	sqlhost=$(cat app/etc/local.xml | grep host | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	sqluser=$(cat app/etc/local.xml | grep username | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	sqlpwd=$(cat app/etc/local.xml | grep password | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	database=$(cat app/etc/local.xml | grep dbname | sed -n 's:.*CDATA\[\(.*\)]].*:\1:p')
	
	# Change to the directory of the Shell-Script
	cd "$(dirname "$0")"
else
	# Change to the directory of the Shell-Script
	cd "$(dirname "$0")"
	
	source 'include/db-credentials.sh'
	echo "Please enter the Name of the Magento-Database:"
	read database
fi

echo "Please enter the Name of the VHost WITHOUT http(s):// (Example: testhost.localhost)"
read vhost 


source 'include/magento/db-develop-changes.sh'



if [ "$ERR" != "" ]
then
	echo $ERR
	echo "Database-Values are not valid or Database is not running. Script stops now ..."
	exit
else
	echo "done!"
fi 

source 'include/magento/db-admin-exists.sh'
