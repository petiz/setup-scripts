#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi


webroot="/home/${_user}/htdocs"

if [ ! -n "$projectfolder" ]; then
    echo "Please enter the foldername for this project inside the webroot WITHOUT final slash (/)"
    read projectfolder
fi

if [ -f "/etc/apache2/sites-available/$projectfolder.conf" ]
then
	echo "Vhost-Dateiname \"$projectfolder\" bereits vorhanden, bitte geben Sie einen Alternativnamen ein"
	read vhost_filename
else
	vhost_filename="$projectfolder"
fi

if [ ! -n "$vhost" ]; then
    echo "Please enter name of vhost WITHOUT \"http(s)://\" (Example: testsystem.localhost)"
    read vhost
fi

if [ ! -n "$subfolder" ]; then
    echo "Please enter the subfolder-name for this project where the vhost points to  (default: \"\")"
    read subfolder
fi

echo "Please choose the PHP-Version for this vhost:"
echo "Enter \"1\" for Version 5.6"
echo "Enter \"2\" for Version 7.2"
echo "Enter \"3\" for Version 7.4"
echo "Enter \"4\" for Version 8.1"
echo "Enter \"5\" for Version 8.3"
echo "Enter \"6\" for Version 8.4"

read php_version

if [ "$php_version" == "1" ]; then
    php_version="5.6"
elif [ "$php_version" == "2" ]; then
    php_version="7.2"
elif [ "$php_version" == "3" ]; then
    php_version="7.4"
elif [ "$php_version" == "4" ]; then
    php_version="8.1"
elif [ "$php_version" == "5" ]; then
    php_version="8.3"
elif [ "$php_version" == "6" ]; then
    php_version="8.4"
fi


if [ ! -d "$webroot/$projectfolder" ]
then
    echo "Projectfolder does not exist, Creating ..."
    mkdir "$webroot/$projectfolder"
    chown "${_user}:${_user}" "$webroot/$projectfolder"
fi


if [ "$subfolder" == "" ]
then
	webpath="$webroot/$projectfolder"
else
    if [ ! -d "$webroot/$projectfolder/$subfolder" ]
    then
        echo "Subfolder does not exist, Creating ..."
        mkdir "$webroot/$projectfolder/$subfolder"
        chown -R "${_user}:${_user}" "$webroot/$projectfolder/$subfolder"
    fi
	webpath="$webroot/$projectfolder/$subfolder"
fi


echo "Trying to add vhost-entry to /etc/hosts ..."
echo "127.0.0.1       $vhost" >> /etc/_hosts/local
echo "127.0.0.1       $vhost" >> /etc/hosts
#/home/${_user}/Tools/Shell/createHostsFile.sh leonex local
echo "Done!"

echo "Create self signed certificate ..."

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout "/etc/ssl/localcerts/$vhost.key" -out "/etc/ssl/localcerts/$vhost.crt" -subj "/C=DE/ST=NRW/L=Paderborn/O=unknown/OU=Web-Entwicklung/CN=$vhost"

echo "Add new vhost to apache-config ..."
cp /etc/apache2/sites-available/vhost-template.conf "/etc/apache2/sites-available/$vhost_filename.conf"
sed -i -e "s,{{vhost}},$vhost," "/etc/apache2/sites-available/$vhost_filename.conf"
sed -i -e "s,{{document_root}},$webpath," "/etc/apache2/sites-available/$vhost_filename.conf"
sed -i -e "s,{{php_version}},$php_version," "/etc/apache2/sites-available/$vhost_filename.conf"
ln -s "/etc/apache2/sites-available/$vhost_filename.conf" "/etc/apache2/sites-enabled/$vhost_filename.conf"

touch "$webpath/phpinfo.php"
echo "<?php phpinfo();" >> "$webpath/phpinfo.php"
chown "${_user}:${_user}" "$webpath/phpinfo.php"

/etc/init.d/apache2 restart


echo "Done!"

