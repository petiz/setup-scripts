#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

mkdir -p "/home/$_user/.setup_flags/desktop_enviroment"
mkdir -p "/home/$_user/Downloads"
mkdir -p "/home/$_user/Temp"


if [ "$(cat /etc/os-release | grep Mint)" != "" ]; then
	os="mint"
elif [ "$(cat /etc/os-release | grep Ubuntu)" != "" ]; then
	os="ubuntu"
elif [ "$(cat /etc/os-release | grep Debian)" != "" ]; then
	os="debian"
else
	echo "No compatible linux-distribution found! Exit"
	exit
fi

# ----------------- Ubuntu Tweaks -----------------

if [[ $os == "ubuntu" && ! -f ~/.setup_flags/desktop_enviroment/unbuntu_tweaks ]]; then
	gsettings set org.gnome.desktop.interface show-battery-percentage true
	gsettings set org.gnome.shell.extensions.ding show-home false
	gsettings set org.gnome.desktop.interface clock-show-seconds true
	gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
	gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
	gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
	gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
	gsettings set org.gnome.shell.extensions.dash-to-dock autohide true
	gsettings set org.gnome.mutter check-alive-timeout 30000
	touch ~/.setup_flags/desktop_enviroment/unbuntu_tweaks
else
	echo "ubuntu-tweaks has already been set"
fi

if [ ! -f ~/.setup_flags/desktop_enviroment/energy ]; then
then
	sudo apt install -y tlp flatpak
	flatpak install flathub com.github.d4nj1.tlpui
	touch ~/.setup_flags/desktop_enviroment/tlp
	#gsettings set org.gnome.settings-daemon.plugins.power sleep-display-ac 300  # 300 Sekunden (5 Minuten)
	#gsettings set org.gnome.settings-daemon.plugins.power sleep-display-battery 180  # 180 Sekunden (3 Minuten)

else
	echo "Energy Consumption is already optimized"
fi


# ----------------- SWAP Changes  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/swap_size ]; then
	echo "- Increasing Swap-Size to 32GB && Swapiness to 40 -"
	sudo swapoff -a
	sudo dd if=/dev/zero of=/swapfile bs=1024M count=32
	sudo chmod 0600 /swapfile
	sudo mkswap /swapfile
	sudo swapon /swapfile
	echo "/swapfile swap swap defaults 0 0" | sudo tee -a /etc/fstab
	echo "vm.swappiness=40" | sudo tee -a /etc/sysctl.conf
	sudo sysctl vm.swappiness=30
	touch ~/.setup_flags/desktop_enviroment/swap_size

else
	echo "Swap Configuration has already been modified"
fi

# ----------------- Idle Setup (Idle also when external screens are connected) -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/idle_setup ]; then
	echo "Idle Setup"
	if [ -f /etc/systemd/logind.conf ]; then
		echo "HandleLidSwitchExternalPower=suspend" | sudo tee -a /etc/systemd/logind.conf
		echo "HandleLidSwitchDocked=suspend" | sudo tee -a /etc/systemd/logind.conf
		touch ~/.setup_flags/desktop_enviroment/idle_setup
	fi
else
	echo "Idle-Setup has already been configured"
fi


# ----------------- Reduce timeout  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/timeout ]; then
	echo "Reduce timeout for processes during shutdown to 10s"
	if [ -f /etc/systemd/system.conf ]; then
		echo "DefaultTimeoutStopSec=30s" | sudo tee -a /etc/systemd/system.conf
		systemctl daemon-reload
		touch ~/.setup_flags/desktop_enviroment/timeout
	fi
else
	echo "Shutdown timeout has been already reduced"
fi

# ----------------- Virtualbox -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/virtualbox ]; then
	sudo apt --yes install virtualbox virtualbox-guest-additions-iso
	sudo usermod -aG vboxusers $(whoami)
	touch ~/.setup_flags/desktop_enviroment/virtualbox
else
	echo "virtualbox is already installed"
fi

if [ ! -f ~/.setup_flags/desktop_enviroment/common_software ]; then
	
	echo "Installing common software, inclusive git & bash-completion"
	
	sudo apt-get install -y curl openvpn gdebi inotify-tools git bash-completion git-flow sshpass samba htop sshfs ydotool > /dev/null
	sudo apt-get install -y imagemagick ghostscript pdftk pdfgrep pikepdf-doc tesseract-ocr python3-pip ocrmypdf tesseract-ocr-deu jbig2 > /dev/null
	sudo apt-get install -y imagemagick parallel ghostscript qpdf unpaper tesseract-ocr tesseract-ocr-deu
	sudo apt-get install -y openssl sendmail
	
	# Worktimer
	sudo apt-get install -y sox libinput-tools

	if [ ! -f /usr/bin/git ]; then
		echo "Initial packages could not be installed. Please check internet-connection"
		exit
	fi
	
	sudo apt-get install -y geany vlc keepassxc meld filezilla tilix gparted thunderbird thunderbird-locale-de
	sudo snap install --classic signal-desktop 
	sudo snap install --classic spotify 
	sudo snap install --classic phpstorm 
	touch ~/.setup_flags/desktop_enviroment/common_software	
fi

if [ ! -f ~/.setup_flags/desktop_enviroment/setup_scripts ]; then
	mkdir -p ~/Setup-Scripts
	git clone -q git@gitlab.com:petiz/setup-scripts.git ~/Setup-Scripts
	touch ~/.setup_flags/desktop_enviroment/setup_scripts
	echo "Setup-Scripts-Repository has been successfully cloned"
else
	echo "Setup-Scripts-Repostory has already been cloned"
fi

# ----------------- Scanner Setup  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/scanner ]; then
	echo "- install scanner-software && add current user to scanner-group -"
	sudo apt-get install -y simple-scan
	sudo usermod -aG scanner $(whoami)
	touch ~/.setup_flags/desktop_enviroment/scanner
else
	echo "Scanner-configuration has already been done"
fi

# ----------------- Dualboot Time Setup  -----------------
# DISABLED
touch ~/.setup_flags/desktop_enviroment/dualboot
if [ ! -f ~/.setup_flags/desktop_enviroment/dualboot ]; then
	echo "- Dualboot Configuration -"
	timedatectl set-local-rtc 1
	sudo timedatectl set-local-rtc 1
	touch ~/.setup_flags/desktop_enviroment/dualboot
else
	echo "Dualboot-Configuration has already been done"
fi


# ----------------- Worktimer Setup  -----------------
if [ ! -f ~/.setup_flags/desktop_enviroment/worktimer ]; then
	echo "- WorkTimer Configuration -"
	
	#sudo sed -i "s/env_reset/env_reset,timestamp_timeout=65/g" /etc/sudoers
	sudo apt-get install -y nscd
	echo "" | sudo tee -a /etc/hosts
	echo "# -- Worktimer START --" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 store.steampowered.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.gamestar.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.notebookcheck.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.keyforsteam.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 secure.scalable.capital" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 de.scalable.capital" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 facebook.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.facebook.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 idealo.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.idealo.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 youtube.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.youtube.com" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 mydealz.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.mydealz.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 amazon.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.amazon.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 ebay.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.ebay.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 kleinanzeigen.de" | sudo tee -a /etc/hosts
	echo "# 127.0.0.1 www.kleinanzeigen.de" | sudo tee -a /etc/hosts	
	echo "# -- Worktimer ENDE --" | sudo tee -a /etc/hosts
	echo "" | sudo tee -a /etc/hosts
	touch ~/.setup_flags/desktop_enviroment/worktimer
else
	echo "WorkTimer-Configuration has already been done"
fi


if [ ! -f ~/.setup_flags/desktop_enviroment/music ]; then

	if [[ -d ~/Musik ]] && [[ $(du -sk ~/Musik | cut -f 1) -lt 100 ]]; then
		echo "lösche Default-Ordner: Musik"
		rm -r ~/Musik
		ln -s ~/Cloud/Musik/Archiv ~/Musik
		ln -s ~/Cloud/Musik/Import ~/Musik-Import
	fi
	touch ~/.setup_flags/desktop_enviroment/music
else
	echo "Music-Symlinks had already been set"
fi
