#!/bin/bash

if [ ! -f ~/backup/dump.tar ]; then
	echo "Please copy dump-file into ~/backup/ and execute script there"
	exit
fi


mkdir -p ~/.setup_flags/desktop_enviroment
mkdir -p ~/old


if [ -d ~/htdocs ]; then
	mv ~/htdocs ~/old/htdocs
fi
if [ -d ~/Teamspeak ]; then
	mv ~/Teamspeak ~/old/Teamspeak
fi
if [ -d ~/.ts3client ]; then
	mv ~/.ts3client ~/old/.ts3client
fi
if [ -d ~/.pwfile ]; then
	mv ~/.pwfile ~/old/.pwfile
fi
if [ -d ~/VirtualBox\ VMs ]; then
	mv ~/VirtualBox\ VMs ~/old/VirtualBox\ VMs
fi
if [ -d ~/.config/VirtualBox ]; then
	mv ~/.config/VirtualBox ~/old/VirtualBox
fi
if [ -d ~/Musik ]; then
	mv ~/Musik ~/old/Projekte
fi
if [ -d ~/.ssh ]; then
	mv ~/.ssh ~/old/.ssh
fi
if [ -f ~/.config.ovpn ]; then
	mv ~/.config.ovpn ~/old/.config.ovpn
fi
if [ -f ~/.pwfile_xc ]; then
	mv ~/.pwfile_xc ~/old/.pwfile_xc
fi


ERR=$((tar -xf dump.tar Musik) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi

ERR=$((tar -vf dump.tar --delete Musik) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi

ERR=$((tar -xf dump.tar htdocs) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi

ERR=$((tar -vf dump.tar --delete htdocs) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi


ERR=$((tar -xf dump.tar Cloud) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi

ERR=$((tar -vf dump.tar --delete Cloud) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi


ERR=$((tar -xf dump.tar) 2>&1) 

if [ "$ERR" != "" ]; then
	echo "Error: $ERR"; exit
fi


#rm ~/.config/composer 2> /dev/null

mv ~/backup/Cloud ~/Cloud
mv ~/backup/Musik ~/Musik
mv ~/backup/.vpn ~/.vpn

mv ~/backup/htdocs ~/htdocs
mv ~/backup/Teamspeak ~/Teamspeak
mv ~/backup/.ts3client ~/.ts3client
mv ~/backup/composer ~/.config/composer
mv ~/backup/Composer-Phar-Files ~/Composer-Phar-Files

mv ~/backup/.ssh ~/.ssh
mv ~/backup/.config.ovpn ~/.config.ovpn
mv ~/backup/.lx_config.ovpn ~/.lx_config.ovpn
mv ~/backup/.pwfile ~/.pwfile

#mv ~/backup/.thunderbird ~/.thunderbird
#mv ~/backup/VirtualBox\ VMs ~/VirtualBox\ VMs
#mv ~/backup/VirtualBox ~/.config/VirtualBox

echo "Backup restored successfully"
touch ~/.setup_flags/desktop_enviroment/backup_restored
