#!/bin/bash

admin_exists=$(mysql --login-path=client $database -e "select user_id from ""$table_prefix""admin_user where username = 'admin';")


if [ "$admin_exists" == "" ] 
then

	if [[ ! -v force_admin_exists ]]
	then
		echo "Admin-User (Username: admin) does not exist. Should i create one default Admin-User now? ( yes / no )"
		read createadmin
    fi
    
    if [[ "$createadmin" == "yes" || -v force_admin_exists ]]
    then
    
        echo "Creating new Admin-User ..."

    	mysql --login-path=client $database -e "
		INSERT INTO admin_user (
			firstname,
			lastname,
			email,
			username,
			password, 
			lognum, 
			reload_acl_flag, 
			is_active, 
			extra, 
			rp_token_created_at
		) VALUES (
			'Testaccount',
			'Testaccount',
			'test@test.de',
			'admin',
			md5('test12345'), 
			0, 
			0, 
			1, 
			'',
			NOW() );
			
		insert into admin_role
			select
			(select max(role_id) + 1 from admin_role) role_id,
			(select role_id from admin_role where role_name = 'Administrators') parent_id,
			2 tree_level,
			0 sort_order,
			'U' role_type,
			(select user_id from admin_user where username = 'admin') user_id,
			'admin' role_name"  		
    fi
else
	echo "Updating password of admin-User ..."
	mysql --login-path=client $database -e "update ""$table_prefix""admin_user set password = md5('test12345') where username = 'admin';"
fi 

echo "Done!"
