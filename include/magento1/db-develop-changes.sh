changes="
 
  # Do not merge JS and CSS-Files
  update ""$table_prefix""core_config_data set value = '0' where path = 'dev/css/merge_css_files';
  update ""$table_prefix""core_config_data set value = '0' where path = 'dev/js/merge_files';
  
  # Do not use HTTPS-Connections and Form-Keys (always show controller in Adminpanel)
  #update core_config_data set value = '0' where path = 'admin/security/use_form_key';
  delete from ""$table_prefix""core_config_data where path = 'admin/security/use_form_key';
  delete from ""$table_prefix""core_config_data where path = 'web/cookie/cookie_domain';
  insert into ""$table_prefix""core_config_data (scope,scope_id,path,value) values ('default',0,'admin/security/use_form_key','0');

  update ""$table_prefix""core_config_data set value = '0' where path = 'web/secure/use_in_frontend';
  update ""$table_prefix""core_config_data set value = '0' where path = 'web/secure/use_in_adminhtml';
  
  # Activating Flat-Indexes for Products and Catalog
  update ""$table_prefix""core_config_data set value = '1' where path = 'catalog/frontend/flat_catalog_category';
  update ""$table_prefix""core_config_data set value = '1' where path = 'catalog/frontend/flat_catalog_product';
  
  # Do not send any E-Mail Copies to our customers while testing orderstable_prefix
  update ""$table_prefix""core_config_data set value = '' where path = 'sales_email/order/copy_to';
  update ""$table_prefix""core_config_data set value = '' where path = 'sales_email/invoice/copy_to';
  update ""$table_prefix""core_config_data set value = '' where path = 'sales_email/creditmemo/copy_to';
  update ""$table_prefix""core_config_data set value = '' where path = 'sales_email/creditmemo_comment/copy_to';
 
  # Deactivating Google-Tracking
  update ""$table_prefix""core_config_data set value = '0' where path = 'google_tag_manager/settings/is_enabled';
  update ""$table_prefix""core_config_data set value = '' where path = 'google_tag_manager/settings/container_public_id';
  update ""$table_prefix""core_config_data set value = '' where path = 'analyse/googleanalytics/account_id';
  update ""$table_prefix""core_config_data set value = '0' where path = 'google/analytics/active';

  # Deactivating TrustedShops
  update ""$table_prefix""core_config_data set value = '' where path = 'trustedrating/status/trustedrating_active';
  update ""$table_prefix""core_config_data set value = '' where path = 'trustedrating/data/trustedrating_id';
  update ""$table_prefix""core_config_data set value = '' where path = 'buyerprotection/data/trustedshops_id';

  
  # Deactivating caches 
  update ""$table_prefix""core_cache_option set value = '0';
  
  # Deactivating wysiwyg
  update ""$table_prefix""core_config_data set value = 'hidden' where path = 'cms/wysiwyg/enabled';
  
  #Do not use seperate Skin- or Media URLs
  update ""$table_prefix""core_config_data set value = '{{unsecure_base_url}}skin/' where path like '%skin_url';
  update ""$table_prefix""core_config_data set value = '{{unsecure_base_url}}media/' where path like '%media_url';
  update ""$table_prefix""core_config_data set value = '{{unsecure_base_url}}js/' where path like '%js_url';

  # Eventually remove external skin and js-urls
  #remove from ""$table_prefix""core_config_data where path = ''
"

if  [ "$vhost" != "" ]
then

	changes+="
  		# Update BASE-URL's to Virtual-Host
  		update ""$table_prefix""core_config_data set value = 'http://$vhost/' where path = 'web/unsecure/base_url';
  		update ""$table_prefix""core_config_data set value = 'http://$vhost/' where path = 'web/secure/base_url'; 
		"  
fi

echo "Setting Test-Enviroment Settings in Database ..."
mysql --login-path=client $database -e "$changes"
echo "Done!"
