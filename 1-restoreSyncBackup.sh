#!/bin/bash


if [[ -d ~/.ssh ]] && [[ $(du -sk ~/.ssh | cut -f 1) -lt 100 ]]; then
	echo "lösche Default-Ordner: SSH"
	rm -r ~/.ssh
fi

# Files

if [ ! -f ~/.lx_config.ovpn ]; then
	echo "Kopiere: .lx_config.ovpn"
	cp .lx_config.ovpn ~/
else
	echo "Datei \".lx_config.ovpn\" existiert bereits"
fi

if [ ! -f ~/.config.ovpn ]; then
	echo "Kopiere: .config.ovpn"
	cp .config.ovpn ~/
else
	echo "Datei \".config.ovpn\" existiert bereits"
fi

# Small folders

if [ ! -d ~/.pwfile ]; then
	echo "Kopiere: .pwfile"
	cp -R .pwfile ~/
else
	echo "Order \".pwfile\" existiert bereits"
fi

if [ ! -d ~/.thunderbird ]; then
	echo "Kopiere: .thunderbird"
	cp -R .thunderbird ~/
else
	echo "Order \".thunderbird\" existiert bereits"
fi

if [ ! -d ~/.ssh ]; then
	echo "Kopiere: .ssh"
	cp -R .ssh ~/
else
	echo "Order \".ssh\" existiert bereits"
fi

if [ ! -d ~/Composer-Phar-Files ]; then
	echo "Kopiere: Composer-Phar-Files"
	cp -R Composer-Phar-Files ~/
else
	echo "Order \"Composer-Phar-Files\" existiert bereits"
fi

if [ ! -d ~/.config/composer ]; then
	echo "Kopiere: /config/composer"
	cp -R .config/composer ~/.config/
else
	echo "Order \"./config/composer\" existiert bereits"
fi

if [ ! -d ~/.config/VirtualBox ]; then
	echo "Kopiere: .config/VirtualBox"
	cp -R .config/VirtualBox ~/.config/
else
	echo "Order \"./config/VirtualBox\" existiert bereits"
fi



# Large folders

if [ ! -d ~/Cloud ]; then
	echo "Kopiere: Cloud"
	cp -Rd Cloud ~/
else
	echo "Order \"Cloud\" existiert bereits"
fi

if [ ! -d ~/htdocs ]; then
	echo "Kopiere: htdocs"
	cp -Rd htdocs ~/
else
	echo "Order \"htdocs\" existiert bereits"
fi

if [ ! -d ~/VirtualBox\ VMs ]; then
	echo "Kopiere: VirtualBox VMs"
	cp -Rd VirtualBox\ VMs ~/
else
	echo "Order \"VirtualBox VMs\" existiert bereits"
fi

