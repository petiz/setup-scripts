#!/bin/bash

if [ -n "$1" ]; then
	_user="$1"
else
	_user="$USER"
fi

if [ ! -d ~/.ssh ]; then
	echo "Please restore Backup first (restoreBackup.sh)"
	echo "Otherwise Tools cannot be checked out"
	exit
fi

if [ ! -d ~/Setup-Scripts/src ]; then
	echo "Setup-Scripts-Source Folder is missing"
	echo "Please run 1-initEnviroment.sh first"
	exit
fi

mkdir -p "/home/$_user/.setup_flags/development_enviroment"
mkdir -p "/home/$_user/PHP-BIN"

while [ true ]; do
	
	echo "Please choose your enviroment. Press \"2\" for Ubuntu/Mint (Desktop) or \"7\" for Debian (Server)"
	read enviroment_id
	
	if [ $enviroment_id == "2" ]; then
		enviroment="desktop"
		php_versions=("7.4" "8.1" "8.3" "8.4")
		break
	elif [ $enviroment_id == "7" ]; then
		enviroment="server"
		php_versions=("5.6" "7.2" "7.4")
		break
	fi
done

if [ "$(cat /etc/os-release | grep Mint)" != "" ]; then
	os="mint"
elif [ "$(cat /etc/os-release | grep Ubuntu)" != "" ]; then
	os="ubuntu"
elif [ "$(cat /etc/os-release | grep Debian)" != "" ]; then
	os="debian"

else
	echo "No compatible linux-distribution found! Exit"
	exit
fi


# ----------------- Checkout Tools  -----------------
if [ ! -f ~/.setup_flags/development_enviroment/tools ]; then
	echo "- Checkout Tools -"
	mkdir -p "/home/$_user/Tools"
	#cd "/home/$_user/Tools"
	git clone -q git@gitlab.com:petiz/tools.git ~/Tools
	touch ~/.setup_flags/development_enviroment/tools

else
	echo "Tools have already been checked out"
fi


# ----------------- add Repository for older php-versions (5.6, 7.4 etc..) -----------------
if [ ! -f ~/.setup_flags/development_enviroment/php_repository ]; then

	echo "- add Repository for older php-versions (5.6, 7.4 etc..) -"
	sudo touch /etc/apt/sources.list.d/ondrej-ubuntu-php-kinetic.list
	echo "deb [trusted=yes] https://ppa.launchpadcontent.net/ondrej/php/ubuntu/ jammy main" | sudo tee -a /etc/apt/sources.list.d/ondrej-ubuntu-php-kinetic.list
	sudo gdebi --non-interactive ~/Setup-Scripts/libicu70_70.1-2ubuntu1_amd64.deb
	sudo apt update
	touch ~/.setup_flags/development_enviroment/php_repository
	
else
	echo "Repository for older php-versions has already been added"
fi

# Check if extra php-packages are available
sudo apt-get -y install php7.4 2> /dev/null

if [ "$(dpkg -s php7.4 2> /dev/null)" == "" ]; then
	echo "Zusätzliche Paketquellen funktionieren nicht, bitte prüfen!"
	exit
fi


# ----------------- install necessary apache-packages -----------------
if [ ! -f ~/.setup_flags/development_enviroment/apache_packages ]; then
	echo "- install necessary apache-packages -"
	sudo apt-get install -y apache2 libapache2-mod-fcgid > /dev/null
	sudo apt-get install -y mariadb-server > /dev/null
	touch ~/.setup_flags/development_enviroment/apache_packages
else
	echo "Apache packages has already been installed"
fi

# ----------------- install necessary php-packages -----------------
if [ ! -f "/home/$_user/.setup_flags/development_enviroment/php_packages" ]; then
	echo "install global php-packages incl. redis-server"
	sudo apt-get install -y php-common imagemagick php-imagick php-redis php-xdebug redis libmcrypt-dev php-intl php-pear make > /dev/null
	
	if [ $? -ne 0 ]; then
		echo "Necessary php-packages couldn't be installed. Please check"
		exit 1
	fi

	touch "/home/$_user/.setup_flags/development_enviroment/php_packages"
fi

for v in "${php_versions[@]}"
do
	if [ ! -f "/home/$_user/.setup_flags/development_enviroment/php${v}_packages" ]; then
		echo "Installing essential packages for php ${v}"
		sudo apt-get install -y "php${v}" "php${v}-fpm" "php${v}-common" "php${v}-mysql" "php${v}-redis" "php${v}-mbstring" "php${v}-xdebug" "php${v}-sqlite3" "php${v}-opcache" "php${v}-curl" "php${v}-xml" "php${v}-cli" "php${v}-gd" "php${v}-zip" "php${v}-intl" "php${v}-readline" "php${v}-bcmath" "php${v}-soap"
		sudo apt-get install -y "php${v}-json"

		if [[ "$v" == "7.4" || "$v" == "7.2" ]]; then
			sudo apt-get install -y php${v}-mcrypt
		fi
		
		if [ -f "/etc/php/${v}/fpm/pool.d/www.conf" ]
		then
			sudo cp "/etc/php/${v}/fpm/pool.d/www.conf" "/etc/php/${v}/fpm/pool.d/www.conf.backup"
			sudo sed -i "s/www-data/$_user/g" "/etc/php/${v}/fpm/pool.d/www.conf"
			mkdir -p "/home/$_user/PHP-BIN/${v}"
			ln -s "/usr/bin/php${v}" "/home/$_user/PHP-BIN/${v}/php"
			touch "/home/$_user/.setup_flags/development_enviroment/php${v}_packages"
			
		else
			echo "PHP-FPM Service is missing. Please check apt-install before proceed with process"
			exit
		fi

	else
		echo "Packages for PHP has been already installed (version $v)"
	fi
	

done


# ----------------- customize apache-settings for fpm-usage and custom vhost-scripts -----------------
if [ ! -f ~/.setup_flags/development_enviroment/customize_apache ]; then

	echo "- install necessary apache-packages -"
	echo "- customize apache-settings for fpm-usage and custom vhost-scripts -"
	
	if [ ! -f /etc/apache2/sites-available/vhost-template.conf ]; then
		sudo cp "/home/$_user/Setup-Scripts/src/vhost-template.conf" /etc/apache2/sites-available/
		sudo chown root:root /etc/apache2/sites-available/vhost-template.conf
	fi
	
	if [ ! -d /etc/ssl/localcerts ]; then
		sudo mkdir -p /etc/ssl/localcerts
	fi
	
	if [ ! -d "/home/$_user/htdocs" ]; then
		mkdir -p "/home/$_user/htdocs"
	fi

	if [ ! -f /etc/apache2/envvars.backup ]; then
		sudo cp /etc/apache2/envvars /etc/apache2/envvars.backup
		sudo sed -i "s/www-data/$_user/g" /etc/apache2/envvars
	fi
	
	#for v in "${php_versions[@]}"
	#do
	#	echo "Switching php-fpm to new apache-user for php ${v}"
	#	sudo cp "/etc/php/${v}/fpm/pool.d/www.conf" "/etc/php/${v}/fpm/pool.d/www.conf.backup"
	#	sudo sed -i "s/www-data/$_user/g" "/etc/php/${v}/fpm/pool.d/www.conf"
	#done

	sudo a2enmod proxy
	sudo a2enmod proxy_fcgi
	sudo a2enmod setenvif
	sudo a2enmod rewrite
	sudo a2enmod ssl

	for v in "${php_versions[@]}"
	do
		sudo a2enconf "php$v-fpm"
	done

	touch ~/.setup_flags/development_enviroment/customize_apache
else
	echo "Apache has already been customized"
fi

# ----------------- add mysql-user for test -----------------
if [ ! -f ~/.setup_flags/development_enviroment/mysql_user ]; then
	
	if [ $enviroment == "desktop" ]; then
		echo "add MYSQL-User for testing"
		sql_password="test12345"
		sql_user="admin"
		cp "/home/$_user/Setup-Scripts/src/mylogin.cnf" "/home/$_user/.mylogin.cnf"
		chmod 600 "/home/$_user/.mylogin.cnf"
	else
		echo "Insert name for sql-adminuser:"
		read sql_user
		echo "Insert password:"
		read sql_password
	fi;
	
	mysql_adduser="CREATE USER '${sql_user}'@'localhost' IDENTIFIED BY '${sql_password}';"
	mysql_grant_privileges="GRANT ALL PRIVILEGES ON *.* TO $sql_user@localhost;"
	
	sudo mysql -uroot -e "$mysql_adduser"
	sudo mysql -uroot -e "$mysql_grant_privileges"
	sudo mysql -uroot -e "FLUSH PRIVILEGES;";
	
	touch ~/.setup_flags/development_enviroment/mysql_user

else
	echo "MYSQL-User has already been added"
fi


if [ $enviroment == "desktop" ]; then

	# ----------------- set limits and add xdebug to php.ini files -----------------
	echo "set limits and add xdebug to php.ini files"
	xdebug=" xdebug.mode=debug\nxdebug.remote_autostart=Off\nxdebug.remote_port=9000\nxdebug.remote_handler=dbgp\nxdebug.idekey=PHPSTORM\nxdebug.max_nesting_level=10000"


	for v in "${php_versions[@]}"
	do
		if [ ! -f "/home/$USER/.setup_flags/development_enviroment/php${v}_ini_settings" ]; then
			
			echo "Updating php.ini for Version $v"

			if [ ! -f "/etc/php/$v/fpm/php.ini.backup" ]; then
				sudo cp  "/etc/php/$v/fpm/php.ini"  "/etc/php/$v/fpm/php.ini.backup"
			else
				sudo cp  "/etc/php/$v/fpm/php.ini.backup"  "/etc/php/$v/fpm/php.ini"
			fi
			
			# update memory-limit
			sudo sed -i "s/memory_limit = 128M/memory_limit = 1024M/g" "/etc/php/$v/fpm/php.ini"
			# update upload-max-filesize
			sudo sed -i "s/upload_max_filesize = 2M/post_max_size = 100M/g" "/etc/php/$v/fpm/php.ini"
			# update post-max-size
			sudo sed -i "s/post_max_size = 8M/post_max_size = 100M/g" "/etc/php/$v/fpm/php.ini"
			# update max-execution-time
			sudo sed -i "s/max_execution_time = 30/max_execution_time = 60/g" "/etc/php/$v/fpm/php.ini"
			# update display errors
			sudo sed -i "s/display_errors = Off/display_errors = On/g" "/etc/php/$v/fpm/php.ini"
			
			printf "$xdebug" | sudo tee -a "/etc/php/$v/fpm/conf.d/20-xdebug.ini" > /dev/null
			touch "/home/$USER/.setup_flags/development_enviroment/php${v}_ini_settings"
		else
			echo "php.ini has already been modified (version $v)"
		fi
	done

	# ----------------- Clone and install NVM in local user-folder -----------------
	if [ ! -f ~/.setup_flags/development_enviroment/nvm ]; then
		echo "- Clone and install NVM in local user-folder -"
		rm -rf "/home/$_user/nvm" 2> /dev/null
		mkdir -p "/home/$_user/nvm"
		git clone https://github.com/creationix/nvm.git "/home/$_user/nvm"
		bash "/home/$_user/nvm/install.sh" > /dev/null
		touch ~/.setup_flags/development_enviroment/nvm
		sudo apt-get install -y build-essential > /dev/null
	else
		echo "NVM has already been installed"
	fi
fi

# ----------------- leonex hosts -----------------
if [ ! -f ~/.setup_flags/development_enviroment/leonex_hosts ]; then
	echo "- Adding leonex-hosts to hostfile -"
	echo "#---------- LEONEX - Hosts ----------" | sudo tee -a /etc/hosts
	echo "192.168.220.152         	elastic1.int.leonex.de" | sudo tee -a /etc/hosts
	echo "193.39.74.100         	gitlab.int.leonex.de" | sudo tee -a /etc/hosts
	echo "192.168.224.16          	composer.int.leonex.de" | sudo tee -a /etc/hosts
	echo "192.168.220.136         	pma.pbecker.dev.int.leonex.de" | sudo tee -a /etc/hosts   
	echo "192.168.222.213         	file-srv1" | sudo tee -a /etc/hosts

	touch ~/.setup_flags/development_enviroment/leonex_hosts
else
	echo "leonex hosts has already been added"
fi

# ----------------- phpmyadmin -----------------
if [ ! -f ~/.setup_flags/development_enviroment/phpmyadmin ]; then
	echo "- Installing phpmyadmin -"
	sudo cp /home/$_user/Setup-Scripts/src/phpmyadmin.conf /etc/apache2/sites-available/phpmyadmin.conf
	sudo sed -i "s/{{document_root}}/\/home\/${_user}\/htdocs\/phpmyadmin/g" /etc/apache2/sites-available/phpmyadmin.conf
	sudo ln -s ../sites-available/phpmyadmin.conf /etc/apache2/sites-enabled/phpmyadmin.conf
	echo "127.0.0.1               phpmyadmin.local" | sudo tee -a /etc/hosts
	
	if [ ! -d ~/htdocs/phpmyadmin ]; then
		unzip /home/$_user/Setup-Scripts/src/phpmyadmin.zip -d ~/htdocs/
	fi
	
	touch ~/.setup_flags/development_enviroment/phpmyadmin
else
	echo "phpmyadmin has already been installied"
fi

# ----------------- Elasticsearch 7 -----------------
if [ ! -f ~/.setup_flags/development_enviroment/elasticsearch7 ]; then

	sudo apt-get install -y openjdk-8-jre apt-transport-https
	
	if [ ! -f /etc/apt/sources.list.d/elastic-7.x.list ]; then
		curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
		echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
	fi

	sudo apt-get update && sudo apt-get install -y elasticsearch
	sudo usermod -aG elasticsearch "$USER"
	sudo systemctl enable elasticsearch.service
	sudo systemctl start elasticsearch.service
	
	if [ -d /usr/share/elasticsearch/plugins/analysis-icu ]; then
		sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove analysis-icu
	fi
	
	if [ -d /usr/share/elasticsearch/plugins/analysis-phonetic ]; then
		sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove analysis-phonetic
	fi
	
	sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-phonetic
	sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu
	sudo service elasticsearch restart
	
	touch ~/.setup_flags/development_enviroment/elasticsearch7
else
	echo "Elasticsearch 7 has already been installied"
fi

# ----------------- add certbot -----------------
if [ $enviroment == "server" ] && [ $os == "debian" ]; then
	if [ ! -f ~/.setup_flags/development_enviroment/certbot ]; then
		echo "- Installing certbot -"
		echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" | sudo tee /etc/apt/sources.list.d/backports.list
		sudo apt-get update
		sudo apt-get install -y certbot python-certbot-apache -t stretch-backports > /dev/null
		touch ~/.setup_flags/development_enviroment/certbot
	fi
fi
	
for v in "${php_versions[@]}"
do
	sudo /etc/init.d/php$v-fpm restart
done

sudo /etc/init.d/apache2 restart


# ----------------- git configuration -----------------
if [ ! -f ~/.setup_flags/development_enviroment/git ]; then
	git config --global user.email "git@petiz.de"
	git config --global user.name "Peter Becker"
	touch ~/.setup_flags/development_enviroment/git
else
	echo "git has already been configurated"
fi
	
# ----------------- Inotify Watches Limit for Phpstorm -----------------
if [ ! -f ~/.setup_flags/development_enviroment/inotify_watches_limit ]; then
	echo "- Changing inotify watches limit for phpstorm -"
	echo "fs.inotify.max_user_watches = 524288" | sudo tee -a /etc/sysctl.conf
	sudo sysctl -p --system
	touch ~/.setup_flags/development_enviroment/inotify_watches_limit
else
	echo "inotify_watches_limit has already been modified"
fi

# ----------------- add additional aliases -----------------
if [ ! -f ~/.setup_flags/development_enviroment/aliases_setup_scripts ]; then
	echo "- add additional aliases -"
	setup_aliases="\n\nif [ -f /home/$_user/Setup-Scripts/_aliases.sh ]; then\n    . /home/$_user/Setup-Scripts/_aliases.sh\nfi"
	printf "$setup_aliases" >> ~/.bashrc
	touch ~/.setup_flags/development_enviroment/aliases_setup_scripts
else
	echo "Additional aliases has already been added"
fi

# ----------------- add additional aliases -----------------
if [ ! -f ~/.setup_flags/development_enviroment/aliases_tools ]; then
	echo "- add additional aliases -"
	setup_aliases="\n\nif [ -f /home/$_user/Tools/Shell/_aliases.sh ]; then\n    . /home/$_user/Tools/Shell/_aliases.sh\nfi"
	printf "$setup_aliases" >> ~/.bashrc
	touch ~/.setup_flags/development_enviroment/aliases_tools
else
	echo "Additional aliases has already been added"
fi

# ----------------- postfix -----------------
if [ ! -f ~/.setup_flags/development_enviroment/postfix ]; then
	echo "- setup postfix -"	
	sudo apt-get install -y postfix mailutils claws-mail claws-mail-fancy-plugin
	echo "virtual_alias_maps = regexp:/etc/postfix/virtual-regexp" | sudo tee -a /etc/postfix/main.cf
	echo "${_user}:        ${_user}@localhost" | sudo tee -a /etc/aliases
	echo "/.+@.+/         ${_user}@localhost" | sudo tee -a /etc/postfix/virtual-regexp
	sudo postmap /etc/postfix/virtual-regexp
	sudo /etc/init.d/postfix restart

	touch ~/.setup_flags/development_enviroment/postfix
else
	echo "postfix-relay has already been configured"
fi


# ----------------- default php -----------------
if [ ! -f ~/.setup_flags/development_enviroment/php_default ]; then
	sudo update-alternatives --config php
	touch ~/.setup_flags/development_enviroment/php_default
else
	echo "default php has already been set"
fi

