#!/bin/bash

#rm -rf ~/backup 2> /dev/null

if [ ! -d ./backup ]
then
	mkdir ./backup
fi


#ERR=$((tar -cf ./backup/dump.tar -C ~/.config composer -C ~/snap firefox) 2>&1) 
ERR=$((tar -cf ./backup/dump.tar -C ~/snap firefox signal-desktop -C ~/.config composer -C ~ VirtualBox\ VMs Cloud .pwfile .thunderbird .lx_config.ovpn .config.ovpn .ssh Composer-Phar-Files htdocs Musik) 2>&1) 


if [ "$ERR" != "" ]; then
	echo "Error: $ERR"
	rm -rf backup 2> /dev/null
else
	echo "Backup created successfully"
	cp ~/Setup-Scripts/1-initEnviroment.sh ./backup/1-initEnviroment.sh
	cp ~/Setup-Scripts/2-setupDevelopmentEnviroment.sh ./backup/2-setupDevelopmentEnviroment.sh
	cp ~/Setup-Scripts/restoreBackup.sh ./backup/restoreBackup.sh
fi
	

